\id RUT
\toc1 
\toc2 RUT
\toc3
\ms INTRODUCCIÓN
\ip El libro de Rut es como un suplemento de los Jueces y una introducción a los Reyes. Contiene la encantadora historia de una familia del tiempo de los Jueces. La moabita Rut, peregrina con su suegra Noemí desde el país de Moab a la patria de esta y se casa con Booz, un rico pariente de su marido. Los dos, Booz y Rut, aparecen en la genealogía de Cristo (Mateo 1, 5).
\ip No se sabe exactamente, cuándo se escribió esta preciosa historia del tiempo de los Jueces, que trata de los antepasados de David. Muy probable es la hipótesis de que fuera escrita en tiempos de este, y se supone que su autor es aquel que escribió el primer libro de los Reyes, tal vez el profeta Samuel.
\ip Nos ofrece un hermoso ejemplo de la divina Providencia que todo lo dispone y hace que concurran aun los menores sucesos al cumplimiento de sus mayores designios. Nos pone ante los ojos un modelo de singular piedad y religión, tanto en Rut como en su suegra Noemí, y nos deja ver en Booz, no solo un modelo de israelita, sino también un miembro de la real estirpe, de la cual nació Nuestro Señor Jesucristo.
\ip Puede verse en este librito también una recomendación del matrimonio levirático (Deuteronomio 25, 5), ya sea el levirato propiamente dicho, ya sea el levirato en sentido amplio, como es el de Booz con Rut.
\c 1
\s Elimelec y su familia
\p
\v 1 \f + \fr 1. \ft \it Los jueces: La Vulgata dice: un juez,\it* o sea, uno de los jueces. Por aquí se ve que esta encantadora historia ha de ubicarse en tiempos de los Jueces, alrededor del año 1150 a. C., poco antes del período de los Reyes. Fue escrita bajo el reinado de David, pues el árbol genealógico que presenta el autor en 4, 18-20, termina con el rey David.\f*Al tiempo en que gobernaban los Jueces, hubo una carestía en el país; y partió un hombre de Betlehem de Judá para habitar en los campos de Moab, él, su mujer y sus dos hijos.
\v 2 \f + \fr 2. \ft \it Efrateos: de Efrata, nombre antiguo de Betlehem (Belén). Cf. Génesis 35, 16-19; 48, 7; Miqueas 5, 2. Moab:\it* país situado al este del Mar Muerto; su límite septentrional era en tiempo de Moisés el Antón; más tarde se extendió más hacia el norte.\f*El hombre se llamaba Elimelec, su mujer, Noemí, y los dos hijos, Mahalón y Quelión. Eran efrateos de Betlehem de Judá. Llegados a los campos de Moab vivieron allí.
\v 3 Murió Elimelec, marido de Noemí, y se quedó ella sola con sus dos hijos,
\v 4 \f + \fr 4. \ft \it Tomaron mujeres moabitas;\it* lo cual estaba prohibido. Los moabitas no podían entrar en la comunidad del pueblo de Dios (Deuteronomio 23, 3). Este pasaje es un argumento en favor de la autoridad histórica de la narración. Ningún autor se habría atrevido a introducir a una mujer pagana y moabita como ejemplo de virtud y madre de David. Cf. Mateo 1, 5.\f*los cuales tomaron mujeres moabitas, siendo el nombre de la una Orfá, y el nombre de la otra Rut. Habitaron allí unos diez años;
\v 5 y murieron también esos dos, Mahalón y Quelión, con lo que la mujer quedó privada de sus dos hijos y de su marido.
\s Piedad filial de Rut
\p
\v 6 Ella se levantó con sus nueras, para volverse del país de Moab; porque había oído en los campos de Moab que Yahvé había visitado a su pueblo, dándole pan.
\v 7 Salió pues del lugar donde estaba, y sus dos nueras con ella, y se pusieron en camino para volver a la tierra de Judá.
\v 8 Dijo entonces Noemí a sus dos nueras: “Id, volveos cada una a la casa de su madre. Y Yahvé use de misericordia con vosotras, como la habéis usado vosotras con los difuntos y conmigo.
\v 9 ¡Yahvé os conceda que halléis descanso cada cual en casa de un marido suyo!” Y las besó; mas ellas alzaron la voz y se pusieron a llorar.
\v 10 Y le decían: “No, nosotras iremos contigo a tu pueblo.”
\v 11 \f + \fr 11. \ft Siendo de distinta nación y religión, ellas no podrían casarse en la tierra de Noemí. Esta suegra ejemplar quiere examinar las verdaderas disposiciones de sus nueras y las trata con afecto maternal para que obren libremente.\f*A lo cual replicó Noemí: “Volveos, hijas mías. ¿Para qué queréis ir conmigo? ¿Tengo por ventura más hijos en mi seno que puedan ser vuestros maridos?
\v 12 ¡Volveos, hijas mías, andad! Soy ya demasiado vieja para casarme. Aun cuando yo dijera: Tengo esperanza y esta misma noche tuviera un marido y diera a luz hijos,
\v 13 ¿acaso esperaríais por eso hasta que ellos fuesen grandes? ¿Os abstendríais por ellos de tener marido? No, hijas mías; porque demasiada amarga es para vosotras mi suerte, pues la mano de Yahvé se ha alzado contra mí.”
\v 14 \f + \fr 14. \ft Orfá se vuelve y recae sin duda en el paganismo. La fidelidad de Rut, que se queda no obstante los obstáculos, le depara toda suerte de bienes: perseverancia en la verdadera felicidad en el hogar, y el honor insuperable de ser abuela de Jesucristo, a pesar de no ser del pueblo escogido.\f*Entonces ellas levantando la voz siguieron llorando. Después Orfá besó a su suegra, en tanto que Rut se acogió a ella.
\p
\v 15 Noemí le dijo: “He aquí que tu cuñada ya se ha vuelto a su pueblo y a sus dioses; vuélvete tú también en pos de tu cuñada.”
\v 16 \f + \fr 16. \ft Rut, la moabita, no solo profesa la verdadera fe en el verdadero Dios, sino que jura por el nombre de Él (versículo 17). \it Adonde tú vayas, iré yo.\it* Como Rut, no cesaremos de decir a nuestro Salvador y divino Esposo: Donde Tú morares, moraré yo. Si Tú estás conmigo, esto me basta, pues Tú nos dices, como a San Pablo: “Mi gracia te basta” (II Corintios 12, 9).\f*Rut respondió: “No insistas en que te deje, retirándome de ti: porque adonde tú vayas iré yo, y donde tú mores moraré yo. Tu pueblo será mi pueblo, y tu Dios será mi Dios.
\v 17 Donde tú murieres, moriré yo, y allí seré sepultada. Que Yahvé me castigue de todas maneras si otra cosa que la muerte me separe de ti.”
\s Noemí y Rut llegan a Betlehem
\p
\v 18 Viendo \it (Noemí)\it* que estaba resuelta a ir con ella, dejó de insistirle,
\v 19 y caminaron las dos hasta que llegaron a Betlehem. A su entrada en Betlehem, toda la ciudad se conmovió a causa de ellas, y decían las mujeres: “¿Esta es Noemí?”
\v 20 \f + \fr 20. \ft \it Noemí: La Vulgata agrega el significado del nombre, para hacer resaltar el contraste con Mará\it*. Noemí significa: Hermosa; Mará, Amarga.\f*Pero ella les contestó: “No me llaméis más Noemí; llamadme Mará, porque el Todopoderoso me ha llenado de amargura.
\v 21 \f + \fr 21. \ft En la Liturgia se aplican estas palabras a la Santísima Virgen cuando perdió su Hijo en el Calvario.\f*Colmada salí, y con manos vacías me ha hecho volver Yahvé. ¿Por qué pues me llamáis Noemí, ya que Yahvé ha dado testimonio contra mí, y me ha afligido el Todopoderoso?”
\p
\v 22 \f + \fr 22. \ft Por piedad filial y amor a su suegra, Rut dejó el país de Moab y todo lo que poseía. “Y mirad, ¡qué mérito fue el haber prestado ayuda y consuelo a la desamparada! Del linaje de Rut nació Jesucristo” (San Jerónimo, A Santa Paula).\f*Volvió por lo tanto Noemí, y con ella Rut, la moabita, su nuera, que había dejado el país de Moab. Llegaron a Betlehem a principios de la siega de las cebadas.
\c 2
\s Rut espigando en el campo de Booz
\p
\v 1 Tenía Noemí un pariente por parte de su marido, de la familia de Elimelec, un hombre poderoso y rico, que se llamaba Booz.
\v 2 \f + \fr 2. \ft El derecho de recoger las espigas sobrantes era, en la admirable Ley de Moisés, un privilegio de los extranjeros, huérfanos y viudas. Rut era las tres cosas a la vez (Levítico 19, 9; 23, 22; Deuteronomio 24, 19).\f*Y dijo Rut, la moabita, a Noemí: “Si me permites, iré al campo, y recogeré espigas en pos de aquel en cuyos ojos hallare gracia.” Dijo ella: “Anda, hija mía.”
\p
\v 3 \f + \fr 3. \ft \it Detrás de los segadores:\it* Esta humildad de Rut, que se confirma en el versículo 13, fue sumamente agradable a Dios. El fue quien dispuso esta aparente casualidad, a saber: que el campo fuese de Booz, por donde vinieron a Rut las más grandes bendiciones temporales y eternas. Es lo que promete Jesús: que los últimos serán los primeros (Mateo 19, 30).\f*Fue, pues, y se puso a espigar en el campo detrás de los segadores. Por fortuna dio con la parcela del campo que pertenecía a Booz, de la familia de Elimelec.
\v 4 \f + \fr 4. \ft Esta fórmula de saludo, acostumbrada entre los israelitas, perdura aún hoy en Palestina. Nótese que es la misma que el Ángel usó para saludar a María (Lucas 1, 28). Es la que usa el celebrante del santo Sacrificio al decir “Dominus vobiscum”. Cf. la nota a Jueces 19, 20 s.\f*Y he aquí que Booz vino de Betlehem, y dijo a los segadores: “Yahvé sea con vosotros.” Ellos le contestaron: “Yahvé te bendiga.”
\v 5 Preguntó Booz al criado suyo que era sobrestante de los segadores: “¿De quién es esa joven?”
\v 6 El criado, sobrestante de los segadores, contestó diciendo: “Es una joven moabita que ha vuelto con Noemí de los campos de Moab.
\v 7 \f + \fr 7. \ft \it Este descanso, etc.: San Jerónimo vierte: ni por un momento se ha vuelto a su casa;\it* Bover-Cantera: sin permitirse ni un pequeño descanso.\f*Ella me dijo: “Déjame espigar e ir detrás de los segadores para recoger entre las gavillas.” Así vino y se ha quedado desde la mañana, hasta ahora; este descanso que \it (ahora)\it* se toma en la cabaña es muy corto.”
\s Generosidad de Booz
\p
\v 8 Dijo luego Booz a Rut: “Oye, hija mía, no vayas a espigar a otro campo, ni te apartes de aquí, sino sigue de cerca a mis criadas.
\v 9 Fija tus ojos en el campo donde se siega y anda detrás de ellas: Pues he dado orden a los criados que no te toquen. Y si tienes sed, irás donde están las vasijas y beberás del agua que han sacado los criados.”
\v 10 \f + \fr 10. \ft \it ¿De dónde me viene?,\it* etc.: Expresión usada por Santa Isabel en la Visitación de María (Lucas 1, 43).\f*Entonces ella cayó sobre su rostro, y postrada en tierra le dijo: “¿De dónde me viene el haber hallado gracia a tus ojos para que me mires, siendo como soy extranjera?”
\v 11 Respondió Booz y le dijo: “Me han contado todo lo que has hecho para con tu suegra, después de la muerte de tu marido; y cómo has dejado a tu padre y a tu madre y al país de tu nacimiento, y has venido a un pueblo que no conocías antes.
\v 12 Recompense Yahvé lo que has hecho, y recibas pleno galardón de parte de Yahvé, el Dios de Israel, bajo cuyas alas te has amparado.”
\v 13 Respondió ella: “¡Halle yo gracia a tus ojos, señor mío! Pues tú me has consolado y has hablado al corazón de tu sierva, aunque no soy ni como una de tus criadas.”
\p
\v 14 \f + \fr 14. \ft La gente humilde solía comer el pan mojado en vinagre, costumbre que se observa aún hoy en diversos países del Oriente.\f*Llegada la hora de comer le dijo Booz: “Ven aquí y come del pan, y moja tu bocado en el vinagre.” Ella se sentó al lado de los segadores; y él le dio del grano tostado, del cual ella comió hasta saciarse, y guardó el resto.
\v 15 Y cuando se levantó para seguir espigando, mandó Booz a sus criados, diciendo: “Hasta entre las gavillas podrá ella recoger espigas, no la increpéis;
\v 16 \f + \fr 16. \ft Delicadeza que caracteriza la caridad verdadera. Véase Eclesiástico 18, 17 s.; 29, 15; Mateo 6, 2-4.\f*antes bien, dejad caer para ella algo de las gavillas, abandonándolo atrás para que ella lo recoja; y no la reprendáis.”
\s Cosecha de Rut
\p
\v 17 \f + \fr 17. \ft El \it efa\it* contenía 36,4 litros.\f*Estuvo Rut espigando en el campo hasta la tarde, y cuando batió lo que había recogido, había como una efa de cebada.
\v 18 Cargó con ello y se volvió a la ciudad; y vio su suegra lo que había espigado. Tras esto Rut sacó lo que había guardado después de haberse saciado, y se lo dio.
\v 19 Le preguntó su suegra: “¿Dónde has espigado hoy, y en qué parte has trabajado? Bendito quien te ha mirado.” Dijo entonces a su suegra con quién había trabajado, y agregó: “El hombre con quien hoy he trabajado se llama Booz.”
\v 20 \f + \fr 20. \ft \it Uno de los que tienen la obligación del levirato, literalmente: uno de nuestros redentores. “Redentor”, en hebreo goël\it*, se llamaba el pariente más cercano, el que estaba obligado a casarse con la viuda de su hermano si este no dejaba hijos (Deuteronomio 25, 5-10). La realización se ve en el capítulo 4.\f*Entonces dijo Noemí a su nuera: “¡Bendito sea él de Yahvé! porque no ha dejado de mostrar su bondad, tanto con los vivos como con los muertos.” Y añadió Noemí: “Pariente cercano nuestro es ese hombre; es uno de nuestros parientes, uno de los que tienen la obligación del levirato.”
\v 21 Y dijo Rut, la moabita: “Él me mandó también: Sigue de cerca a mis criados hasta que hayan acabado de segar toda mi cosecha.”
\v 22 Dijo entonces Noemí a Rut, su nuera: “Mejor es, hija mía, que salgas con sus criados, para que no te maltraten en otro campo.”
\p
\v 23 Se acogió para espigar, a las criadas de Booz, hasta terminar la siega de las cebadas y la siega de los trigos. Y habitaba con su suegra.
\c 3
\s Rut a los pies de Booz
\p
\v 1 Le dijo Noemí, su suegra: “Hija mía, ¿no he de buscar para ti un lugar de reposo donde te vaya bien?
\v 2 \f + \fr 2. \ft \it Avienta la cebada en la era:\it* El suceso era este: En la era yacía amontonado el grano mezclado con el tamo. Con el bieldo arrojaba Booz esta mezcla a lo alto contra el viento, él cual se llevaba el tamo, por ser más liviano, mientras el grano, por ser más pesado, caía en la era. Booz elige el tiempo de la noche, para aprovechar la brisa que todas las noches viene del mar. Cf. Mateo 3, 12.\f*Ahora ese Booz, con cuyas criadas tú has estado, es pariente nuestro. Mira, esta noche avienta él la cebada en la era.
\v 3 Lávate, por tanto y úngete, y ponte tus vestidos y baja a la era; mas no te des a conocer al hombre hasta que haya acabado de comer y beber.
\v 4
\f + \fr 4. \ft Noemí sabía que Booz era uno de los parientes obligados a casarse con la viuda de su hijo (cf. 2, 20 y nota), pero sospechando que él, como hombre rico y de edad avanzada, no tomaría por esposa a una viuda pobre y extranjera, recurrió a esta ingenua y al mismo tiempo ingeniosa manera de recordarle su deber. Toda la escena que viene a continuación es un poema de incomparable pureza, que recuerda el caso de Abisag (III Reyes 1) y de Susana y del Cantar de los Cantares; casos que Dios nos ha puesto delante para que su Palabra infinitamente casta (Salmo 11, 7) limpie nuestras perversas intenciones y nos enseñe la rectitud interior. Todo es puro para los puros, dice San Pablo (Tito 1, 15).\f*Y al acostarse él, nota bien el lugar donde se acuesta; luego irás, y le destaparás la parte de los pies, y te acostarás. Él te dirá entonces lo que has de hacer.”
\v 5 Ella le respondió: “Haré todo lo que dices.”
\p
\v 6 Bajó a la era, e hizo todo lo que le había ordenado su suegra.
\v 7 Booz comió y bebió, y se alegró su corazón. Y cuando fue a acostarse al extremo de un montón de gavillas, se acercó ella calladamente, y destapándole la parte de los pies se acostó.
\v 8 A media noche el hombre tuvo un gran susto, porque al darse vuelta, vio que una mujer estaba acostada a sus pies.
\v 9
\f + \fr 9. \ft Rut le pide con las palabras de mayor modestia que la reciba bajo su capa, es decir, su protección y que la tome por esposa para conservar el nombre de su pariente en Israel.\f*Preguntó: “¿Quién eres?” Y ella contestó: “Soy Rut, tu sierva; extiende tu manto sobre tu sierva, porque tú tienes respecto de mí la obligación del levirato.”
\v 10 A lo que dijo él: “¡Bendita seas de Yahvé hija mía! Tu último acto de piedad es mejor que el primero, porque no andas tras los jóvenes, ni pobres, ni ricos.
\v 11 Ahora, hija mía, no temas. Yo haré por ti cuanto me digas; pues todos mis conciudadanos saben que eres una mujer virtuosa.
\v 12
\f + \fr 12. \ft Booz, pensando que había otro pariente más cercano, decide averiguar el asunto, para después cumplir con su deber. Toda su conducta es un ejemplo de rectitud. Noemí pudo ignorar que hubiese otro pariente más cercano.\f*Mas ahora, aunque es cierto que tengo la obligación del levirato, sin embargo hay un pariente más cercano que yo.
\v 13 Pasa la noche, y si él mañana quiere cumplir con su deber de levirato, que lo haga; pero si él no lo hace, lo haré yo. ¡Vive Yahvé! Acuéstate hasta la mañana.”
\p
\v 14 \f + \fr 14. \ft Rut tiene buen cuidado de retirarse antes de la luz del día, para evitar todo escándalo, que podría haber sido entonces gravísimo pecado, aunque ella no hubiera cometido ninguna mala acción. Es este un punto muy serio que un cristiano no debe ignorar según enseñan Jesús (Mateo 18, 6-7) y San Pablo (I Corintios 8, 13).\f*Quedó ella acostada a sus pies hasta la mañana; y se levantó antes de poder distinguir un hombre a otro; porque él dijo: “Nadie sepa que esta mujer vino a la era.”
\v 15 \f + \fr 15. \ft El manto es el velo grande con que las mujeres orientales se cubrían desde la cabeza hasta los pies.\f*Y agregó: “Extiende el manto que traes sobre ti, y tenlo bien.” Ella lo tuvo bien, y él le midió seis \it (medidas)\it* de cebada, que le cargó a cuestas, y ella se fue a la ciudad.
\p
\v 16 Cuando llegó a su suegra, esta preguntó: “¿Qué es lo que has alcanzado, hija mía?” Y Rut le contó todo lo que el hombre le había hecho.
\v 17 Dijo también: “Me ha dado estas seis \it (medidas)\it* de cebada, diciéndome: No vuelvas a tu suegra con las manos vacías.”
\v 18 Dijo \it (la suegra)\it*: “Siéntate, hija mía, hasta que sepas en que va a parar este asunto; porque no descansará ese hombre hasta que lo haya acabado hoy mismo.”
\c 4
\s Gestiones con el pariente más cercano
\p
\v 1 \f + \fr 1. \ft \it Fulano:\it* Todos los que intervienen en esta historia son introducidos con su nombre, menos este villano, que rehusaba cumplir con el deber del levirato.\f*Subió Booz a la puerta \it (de la ciudad)\it* y se sentó allí; y he aquí que pasaba aquel pariente obligado al levirato, de quien Booz había hablado. Le dijo: “Ven aquí y siéntate, fulano.” Y se acercó el hombre y se sentó allí.
\v 2 \f + \fr 2. \ft \it Diez hombres,\it* como testigos del contrato que se iba a realizar.\f*Tomó también diez hombres de los ancianos de la ciudad, y dijo: “Tomad asiento”; y ellos se sentaron.
\v 3 Entonces dijo al pariente obligado al levirato: “Noemí, que ha vuelto de los campos de Moab, vende la porción de campo que era de nuestro hermano Elimelec.
\v 4 He querido informarte de ello y te propongo: Adquiérela delante de los que están aquí sentados y delante de los ancianos de mi pueblo. Si quieres cumplir con el deber del levirato, hazlo; si no, dímelo, para que yo lo sepa; pues tú eres el pariente más cercano; después de ti vengo yo.” Él respondió: “Yo cumpliré con ese deber.”
\v 5 \f + \fr 5. \ft \it Para resucitar el nombre del difunto,\it* significa casarse con la viuda para dar un heredero al pariente muerto. El primogénito procedente del nuevo matrimonio recibía el nombre y la herencia del difunto (Deuteronomio 25, 6). Respecto de la preferencia de los parientes en la venta de los campos, véase Números 36, 3 ss.\f*Le dijo entonces Booz: “Cuando adquieras el campo de manos de Noemí, lo adquirirás también de Rut la moabita, mujer del difunto, para resucitar el nombre del difunto sobre su herencia.”
\v 6 Replicó el obligado al levirato: “No puedo hacerlo, para no perjudicar mi herencia. Ejerce tú ese derecho que tengo yo, pues yo no puedo hacerlo.”
\s Casamiento de Booz con Rut
\p
\v 7 Era costumbre antigua en Israel, en casos de levirato y cambios, que para dar validez a todo acto, el uno se quitaba el zapato y lo daba al otro. Esto servía de testimonio en Israel.
\v 8 Por eso, el hombre obligado al levirato dijo a Booz: “Adquiérelo tú por tú cuenta.” Y se quitó el zapato.
\v 9 Dijo entonces Booz a los ancianos y a todo el pueblo: “Vosotros sois hoy testigos de que yo he adquirido de mano de Noemí todo lo que era de Elimelec, y todo lo que era de Quelión y Mahalón,
\v 10 y que he adquirido también a Rut la moabita, mujer de Mahalón, para que sea mi mujer, a fin de resucitar el nombre del difunto sobre su herencia, y para que el nombre del difunto no se borre de entre sus hermanos, ni de la puerta de su lugar. De eso sois vosotros hoy testigos.”
\v 11 \f + \fr 11. \ft Hermosa fórmula de felicitación para un futuro esposo.\f*Y todo el pueblo que estaba en la puerta, respondió juntamente con los ancianos: “Somos testigos. ¡Haga Yahvé que la mujer que va a entrar en tu casa, sea como Raquel y como Lía, que ambas edificaron la casa de Israel, para que seas poderoso en Efrata y tengas renombre en Betlehem!
\v 12 ¡Venga a ser tu casa como la casa de Fares, que Tamar le dio a Judá, por la descendencia que Yahvé te diere de esta joven!”
\p
\v 13 \f + \fr 13. \ft San Ambrosio ve en Rut una figura de las naciones gentiles y en la incorporación de ella al pueblo de Dios una profecía de la vocación de los gentiles al redil de Cristo.\f*Tomó Booz a Rut, y ella fue su mujer. Entró a ella, y Yahvé le concedió que concibiera y diera a luz un hijo.
\v 14 Entonces decían las mujeres a Noemí: “¡Bendito sea Yahvé, que no te ha negado un redentor el día de hoy! ¡Su nombre sea celebrado en Israel!
\v 15 ¡Que el consuele tu alma y sea el sostén de tu vejez! Pues tu nuera, que te ama y que para ti vale más que siete hijos, ha dado a luz.”
\v 16 \f + \fr 16. \ft Noemí es modelo de abuela como antes lo fue de suegra. En la genealogía de Jesucristo se recuerdan los nombres aquí mencionados. Cf. Mateo 1, 3-6; Lucas 3, 32. Véase I Paralipómenos 2, 5 y 4, 1.\f*Y Noemí tomó al niño, lo puso en su regazo, y le sirvió de aya.
\v 17 Y las vecinas la aclamaron diciendo: “A Noemí le ha nacido un hijo”, y le llamaron Obed. Él fue padre de Isaí, padre de David.
\s Genealogía de David
\p
\v 18 Estas son las generaciones de Fares: Fares engendró a Hesrón;
\v 19 Hesrón engendró a Ram, Ram engendró a Aminadab,
\v 20 Aminadab engendró a Naasón, Naasón engendró a Salmón.
\v 21 Salmón engendró a Booz, Booz engendró a Obed,
\v 22 Obed engendró a Isaí, e Isaí engendró a David.
