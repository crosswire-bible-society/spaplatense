\id NAM
\toc1 
\toc2 NAHÚM
\toc3
\ms INTRODUCCIÓN
\ip \it Nahúm\it* vivió en el siglo VII a. C.; según la tradición judía, bajo el rey Manasés (693-639), o quizá Josías (638-608), y profetizó contra Nínive, capital del reino de los asirios. Fuera de este oráculo no poseemos nada de su actividad profética, la cual está colocada entre la de Isaías, de quien cita varios pasajes (cf. 1, 4 = Isaías 33, 9; 1, 15 = Isaías 52, 7; 3, 5 = Isaías 47, 3 y 9); y la de Jeremías que, a la inversa, cita a nuestro profeta (cf. 1, 13 = Jeremías 30, 8; 3, 5, 13, 17 y 19 = Jeremías 13, 12 ss; 50, 37; 51, 30, etc.).
\ip Lo único que acerca de la vida de Nahúm indica la Sagrada Escritura (Nahúm 1, 1) es el lugar de su nacimiento, pues lo llama elcesco, es decir, de Elkosch, situada, según unos, en Galilea, según otros en Judea, y cuyas ruinas se veían allí todavía en tiempos de San Jerónimo. Menos fundada es la opinión de que naciera en Alkosch, situada cerca de Mosul, donde los nestorianos veneran su sepulcro.
\ip Como Abdías se consagró esencialmente a anunciar la ruina de los idumeos, hijos de Esaú y enemigos envidiosos de Israel, aunque hermanos suyos según la carne, así el fin de la profecía de Nahúm es prevenir a sus lectores contra la poderosa capital asiria, y darles la seguridad de que será destruida la que un día pareció realizar la hazaña —única entre los pueblos gentiles— de convertirse al Dios de Israel (cf. Jonás 3) para caer luego en la apostasía y ser su más terrible enemiga (1, 11 y nota). En tal sentido las profecías de Nahúm y Jonás son correlativas, y cada una releva la gran importancia de la otra en el plan divino. En tiempo de Nahúm, Nínive había ya llevado cautivas a las diez tribus del norte (Israel) en 721, y amenazaba orgullosamente a Jerusalén bajo Senaquerib (IV Reyes 18, 15 s.), a cuya invasión de Judea, milagrosamente frustrada por un ángel (cf. Isaías 36-37), parecería aludir Nahúm en 1, 12 s.
\c 1
\p
\v 1 \f + \fr 1. \ft \it Carga sobre Nínive:\it* Profecía conminatoria contra Nínive. Véase Isaías 13, 1; 14, 28; 15, 1; Jeremías 23, 33, etc.\f*Carga sobre Nínive. Libro de la visión de Nahúm de Elkosch.
\s Castigo de Asiria
\p
\v 2 \f + \fr 2. \ft \it Un Dios celoso:\it* En el Pentateuco (Éxodo 20, 5; Deuteronomio 4, 24) el Señor recibió ya el epíteto de Dios celoso, que es la expresión de su amor a Israel.\f*Yahvé es un Dios celoso y vengador;
\p vengador es Yahvé y lleno de ira.
\p Yahvé ejerce la venganza
\p contra sus adversarios,
\p y guarda rencor a sus enemigos.
\p
\v 3 \f + \fr 3. \ft Como vemos en todo este elocuentísimo pasaje, Dios esperó antes de castigar las maldades de Nínive. La perdonó un siglo antes, en tiempo de Jonás (Jonás 3), cuando ella dio señales de arrepentimiento. Pero aquí la vemos de nuevo “sanguinaria y llena de fraudes” (3, 1 ss.), hecha otra ramera como Babilonia (3, 4), por lo cual, como esta, será arrasada para siempre (versículo 9). San Pedro enseña que la condición del apóstata, que vuelve atrás después de convertirse, es peor que la de antes (II Pedro 2, 20; cf. Mat. 12, 45). Ahora bien, mientras a su pueblo escogido, a pesar de sus repetidas apostasías, Dios le promete siempre una misericordia final y gratuita (véase Jeremías 30, 13 y nota; Romanos 9, 15; 11, 6), no hará lo mismo con la Nínive gentil (Isaías 1, 24-28). Es de notar que esta capital de los asirios, que figura a los enemigos del reino de Dios en los últimos tiempos (Isaías 5, 25; Miqueas 5, 5 y notas), siendo la única pagana que se convirtió al verdadero Dios (Jonás 3, 5), representa en sentido escatológico la apostasía religiosa de la gentilidad (versículo 11), como Babilonia la simboliza en lo político, aspectos ambos que se juntarán en el Anticristo. Cf. II Tesalonicenses 2; Apocalipsis 13.\f*Yahvé es longánimo y grande en poder,
\p y no deja impune \it (al impío)\it*.
\p Marcha Yahvé en el torbellino
\p y en la tempestad,
\p y las nubes son el polvo de sus pies.
\p
\v 4 \f + \fr 4. \ft \it Basán , el Carmelo y el Líbano\it*, son las regiones más amenas y fértiles de Palestina. Se desarrolla aquí un cuadro de la ira del Señor que baja del cielo para mostrar su poder. Véase Éxodo 19, 16 ss.; Salmo 17, 8-16; 67, 8 ss.; Hab. 3, 3, etc.\f*Increpa al mar y lo deja seco,
\p y agota todos los ríos.
\p Faltos de lozanía están Basán y el Carmelo,
\p y el verdor del Líbano se marchita.
\p
\v 5 Delante de Él se estremecen los montes,
\p y se derriten los collados.
\p Ante su faz se conmueve la tierra,
\p el orbe y cuantos en él habitan.
\p
\v 6 ¿Quién podrá subsistir ante su ira?
\p ¿Quién resistir el ardor de su cólera?
\p Se derrama como fuego su indignación,
\p y ante Él se hienden las rocas.
\p
\v 7 \f + \fr 7. \ft \it Yahvé es bueno:\it* La Biblia no es sino el inmenso arsenal de los Hechos de nuestro Padre, donde aprendemos a mirarlo como siempre activo y “dominado por el amor” (Pío XII). Si obramos con Él como un caballero obra con su padre ilustre, viviremos estudiando en las sagradas páginas esas hazañas suyas, para gloriarnos de ellas y pregonarlas. Esto es tenerle a Dios fe, esa fe viva que nos hace obrar por amor (Gal. 5, 6). Los Setenta dicen bellamente; Yahvé es para los que esperan en Él en el día de la tribulación. Como observa un autor, en el Antiguo Testamento, “esperar” o “confiar” equivale en el Nuevo Testamento a “creer” o “tener fe”.\f*Yahvé es bueno,
\p es fortaleza en el día de la tribulación,
\p Él conoce a los que en Él confían,
\p
\v 8 \f + \fr 8. \ft \it Aquel lugar: Nínive. Sus enemigos:\it* los asirios, enemigos de Dios. Las mismas tinieblas perseguirán a los enemigos, porque se entregaron a las tinieblas, amando más las tinieblas que la luz (cf. Juan 3, 19).\f*Con inundación arrolladora
\p destruirá por completo aquel lugar,
\p y las tinieblas perseguirán a sus enemigos.
\s Sentencia contra Nínive
\p
\v 9 \f + \fr 9. \ft \it No surge dos veces la tribulación:\it* La ruina de Nínive será tan completa que no se necesita otro golpe contra ella. Esto se cumplió históricamente (cf. 2, 11 ss.), y Nínive nunca volvió a levantarse después de la destrucción.\f*¿Qué maquináis contra Yahvé?
\p El hace devastación completa,
\p no surge dos veces la tribulación.
\p
\v 10 Pues bien atados entre sí, como espinos,
\p esos embriagados de su vino
\p serán consumidos cual paja enteramente seca.
\p
\v 11 \f + \fr 11. \ft \it De ti: de Nínive. El Que piensa mal contra Yahvé:\it* el asirio. Pero aquí hay más que una alusión histórica. El asirio es figura del enemigo eterno de Dios (véase versículo 3 y nota. Cf. Isaías 5, 25; 30, 28 y 31; 31, 4-8; Salmo 75 y 82 y notas; Miqueas 5, 5 s. y nota, etc.). Pensar mal del Señor es exactamente lo contrario de lo que anotamos en el versículo 7. Es ir contra lo más esencial y primario de la sabiduría (Sabiduría 1, 1; 3, 9 y notas). Es lo propio de la soberbia apóstata que analiza a Dios y lo juzga. Véase II Corintios 10, 5; Col. 2, 8 y notas. Va sin decirlo, que este extravío espiritual, al impedir la gracia que viene de la amistad con Dios, y que Él niega a los soberbios (Santiago 4, 6; I Pedro 5, 5), conduce también a los abismos de la depravación moral que San Pablo señala en los gentiles (Romanos 1, 21-32).\f*De ti salió el que piensa mal contra Yahvé,
\p el que traza designios de iniquidad.
\p
\v 12 \f + \fr 12. \ft Además de ser un oráculo contra Nínive, este verso es también una promesa para Jerusalén. Dios consuela a su pueblo prometiéndole no afligirle en adelante por medio de los asirios. Observan algunos que esta promesa no es absoluta en sentido histórico y se limita a Nínive mientras existió. En realidad, caída esa capital en el año 612 a. C, lo que en adelante sufrió Judá no fue ya por parte de Asiria sino de Babilonia. Por lo demás, la promesa puede también referirse a los asirios en sentido escatológico (versículo 11 y nota).\f*Así dice Yahvé:
\p “Aunque sean sanos y salvos y muy numerosos,
\p con todo serán cortados y desaparecerán.”
\p Te he humillado, pero no te humillaré más.
\p
\v 13 Ahora romperé su yugo \it (que pesa)\it* sobre ti,
\p y haré pedazos tus coyundas.
\p
\v 14 Yahvé ha decretado respecto de ti:
\p “Ya no habrá más posteridad
\p que lleve tu nombre.
\p Exterminaré de la casa de tus dioses
\p las estatuas e ídolos de fundición;
\p y Yo te haré el sepulcro,
\p porque serás consumida muy pronto.”
\p
\v 15 \f + \fr 15. \ft \it Buenas nuevas\it*, etc.: la ruina de Nínive. Mensajeros que vienen de Asiria anunciarán la caída de la ciudad orgullosa y fortísima (cf. Jonás 1, 2 y nota). Su ruina significa la paz para Israel. Alégrese entonces el pueblo, celebre fiestas, y cumpla los votos que hiciera al Señor. Este pasaje recuerda una palabra semejante de Isaías (52, 7) que se refiere a la paz mesiánica. Tiene aquí el mismo sentido que en Isaías. \it Belial:\it* hombre malvado, aquí el asirio.\f*He aquí sobre los montes
\p los pies de aquel que trae buenas nuevas,
\p de aquel que anuncia la paz.
\p Celebra, Judá, tus fiestas,
\p cumple tus votos;
\p que ya no volverá a pasar por ti aquel Belial.
\p Ha sido completamente extirpado.
\c 2
\s Destrucción de Nínive
\p
\v 1 \f + \fr 1. \ft Los que suben son los destructores de la ciudad, los medos y babilonios. Este verso es en el texto hebreo el segundo.\f*Está ya delante de ti el devastador;
\p guarda la plaza fuerte,
\p observa los caminos;
\p fortalece tus lomos,
\p aumenta mucho tus fuerzas.
\p
\v 2 Pues Yahvé restaura la gloria de Jacob,
\p así como la gloria de Israel;
\p porque los saquearon saqueadores
\p que destruyeron sus vástagos.
\p
\v 3 \f + \fr 3 ss. \ft Descripción de los guerreros que asaltan a Nínive. \it Vibran sus lanzas: La Vulgata vierte: Adormecidos (borrachos) sus conductores. Bover-Cantera: blandense los abetos (de las lanzas); Nácar-Colunga: al atacar sus caballos son un torbellino\it*. El sentido es: Los enemigos avanzan rápidamente en irresistible arremetida. El versículo 5 quiere decir que el rey de Nínive se acuerda de sus valientes y los llama para defender la ciudad.\f*Los escudos de sus guerreros
\p están teñidos de rojo,
\p sus valientes vestidos de púrpura;
\p sus carros centellean como acero
\p en el día de la reseña,
\p y vibran sus lanzas.
\p
\v 4 Los carros se precipitan por las calles,
\p atraviesan veloces las plazas;
\p parecen antorchas,
\p corren como relámpagos.
\p
\v 5 Él \it (rey)\it* llama a sus valientes,
\p que se precipitan por los caminos
\p y corren presurosos al muro;
\p se prepara la defensa.
\p
\v 6 \f + \fr 6. \ft \it Las puertas de los ríos:\it* las que dan sobre el Tigris y su afluente; serán tomadas y destruidas lo mismo que el templo, que estaba también junto al río. San Jerónimo lo refiere a la multitud de habitantes.\f*Pero ya se abren las puertas de los ríos,
\p y cae el palacio.
\p
\v 7 \f + \fr 7. \ft Texto muy oscuro. \it Ha sido llevado a cabo... ha sido desnudada: Vulgata: el soldado fue llevado cautivo. Bover-Cantera: es conducida (la ciudad) descubierta. Nácar-Colunga: la reina es desnudada. Otros vierten: Huzab ha sido llevada,\it* tomando a Huzab como reina o diosa de Nínive, idéntica con Zib o Belit.\f*Ha sido llevado a cabo;
\ip (Nínive) ha sido desnudada,
\p es llevada (al cautiverio);
\p sus criadas gimen,
\p como con voz de paloma,
\p y se golpean los pechos.
\p
\v 8 Nínive es desde la antigüedad
\p como un estanque de aguas,
\p las cuales se van.
\p ¡Deteneos, deteneos!
\p pero nadie vuelve.
\p
\v 9 \f + \fr 9. \ft Los vencedores se exhortan mutuamente a saquear la ciudad más rica del mundo.\f*¡Saquead la plata! ¡Saquead el oro!
\p no tienen fin los tesoros,
\p es inmenso el peso de toda suerte
\p de objetos preciosos.
\p
\v 10 Queda vacía, devastada y desolada;
\p se desmayan los corazones
\p y tiemblan las rodillas;
\p se quebrantan todos los lomos,
\p y palidecen los rostros de todos.
\p
\v 11 \f + \fr 11. \ft \it Guarida de los leones:\it* Nínive, de donde los ejércitos salieron para despojar a otros pueblos. Véase Sofonías 2, 13 ss. donde se le profetiza la misma desolación. También el anciano Tobías lo había anunciado diciendo: “Presto sucederá la ruina de Nínive, pues la palabra de Dios no puede faltar” (Tobías 14, 6), lo cual muestra que él conocía ya en tiempo de Salmanasar (Tobías 1, 2 y nota) algún anuncio profético en tal sentido.\f*¿Dónde está la guarida de los leones,
\p el lugar de pasto de los leoncillos?
\p ¿Adónde se han retirado el león,
\p la leona y el cachorro,
\p sin que nadie los espantase?
\p
\v 12 el león que destrozaba lo que necesitaba
\p para sus cachorros,
\p y ahogaba para sus leonas;
\p llenaba sus cubiles de presa
\p y sus guaridas de rapiña.
\p
\v 13 \f + \fr 13. \ft La conquista de Nínive tan claramente profetizada por Nahúm, fue llevada a cabo entre 612 y 604 a. C. por los babilonios y medos, después de una inundación del Tigris que arruinó gran parte de las murallas. Su último rey, el célebre Sardanápalo, pereció en las llamas del palacio que él mismo mandó incendiar. Las ruinas parcialmente excavadas demuestran que Nínive fue saqueada, antes de la destrucción.\f*Heme aquí contra ti,
\p dice Yahvé de los ejércitos;
\p reduciré a humo tus carros,
\p y la espada devorará a tus leoncillos;
\p exterminaré de la tierra tu rapiña,
\p y no será oída más
\p la voz de tus embajadores.
\c 3
\s Los crímenes de Nínive
\p
\v 1 ¡Ay de la ciudad sanguinaria
\p que está toda llena de mentiras y de robo,
\p y nunca suelta la presa!
\p
\v 2 \f + \fr 2 s. \ft Retoma el profeta la descripción de la caída de Nínive. Los conquistadores recorren enfurecidos las calles de la ciudad, dejando tras de sí montones de cadáveres.\f*Estruendo de látigos,
\p y estrépito de ruedas.
\p Caballos que corren y carros que saltan.
\p
\v 3 jinetes erguidos, fulgentes espadas,
\p lanzas relampagueantes.
\p Multitud de traspasados,
\p cadáveres en masa, muertos sin fin.
\p Tropieza la gente con los cuerpos muertos.
\p
\v 4 \f + \fr 4 ss. \ft Nínive fue como una ramera, por cuanto sabía atraer a otros pueblos mediante su enorme influencia política, económica y cultural. Por lo cual Yahvé le da el castigo que se-aplica a las rameras. \it Descubriré las faldas\it*, etc. (versículo 5): Cf. Isaías 47, 2 f.; Jeremías 13, 22 ss.; Oseas 2, 5, etc. Es de notar que en la Sagrada Escritura la fornicación significa el culto de los ídolos; la infidelidad a Dios es sinónimo de adulterio. Este es el único pasaje en que el término se aplica a una ciudad pagana, quizá porque fue también la única convertida por el verdadero Dios con extraordinaria misericordia, como se ve en todo el Libro de Jonás, por lo cual su infidelidad ulterior podía llamarse en realidad apostasía. Compárese sobre Tiro Isaías 23, 16; cf. Ezequiel 16, 29; Oseas capítulos 1-3; Apocalipsis 14, 8; 17, 2; 18, 3; 19, 2.\f*Es a causa de las muchas fornicaciones
\p de la ramera, bella y encantadora,
\p maestra en hechicerías,
\p que con sus fornicaciones
\p esclavizaba a las naciones,
\p y con sus hechizos a los pueblos.
\p
\v 5 Heme aquí contra ti,
\p dice Yahvé de los ejércitos;
\p descubriré las faldas de tu (vestido)
\p hasta sobre tu cara,
\p y mostraré a las naciones tu desnudez,
\p y a los reinos tu vergüenza.
\p
\v 6 Arrojaré sobre ti inmundicias,
\p te cubriré de afrenta
\p y te pondré por espectáculo.
\p
\v 7 Cuantos te vean, retrocederán de ti,
\p diciendo: ¡Destruida está Nínive!
\p ¿Quién tendrá compasión de ella?
\p ¿Dónde buscaré a quien te consuele?
\p
\v 8 \f + \fr 8 ss. \ft \it No-Amón\it* (Vulgata: \it Alejandría de los pueblos\it*). Es esta la ciudad de Tebas, capital del Alto Egipto, conquistada y saqueada por los asirios en el año 664 a. C. En esa época Egipto estaba gobernado por una dinastía de Etiopía, que dominaba también el país de Libia. Alejandría no existía en tiempos de Nahúm. \it Los ríos:\it* el Nilo y sus canales.\f*¿Eres tú acaso mejor que No-Amón,
\p que se sentaba sobre los ríos,
\p que estaba rodeada de aguas,
\p cuyo baluarte era el mar
\p y cuya muralla formaban las aguas?
\p
\v 9 Grandes eran las fuerzas de Etiopía
\p e inmensas las de Egipto;
\p Put y Libia eran sus auxiliares.
\p
\v 10 Pero también ella ha sido deportada,
\p ha sido llevada al cautiverio,
\p y sus niños también fueron estrellados
\p en las encrucijadas de todas las calles;
\p se echaron suertes sobre sus nobles,
\p y fueron cargados de cadenas todos sus grandes.
\p
\v 11 \f + \fr 11. \ft \it Te embriagarás:\it* beberás el cáliz de la cólera del Señor. Metáfora frecuente. Véase Isaías 51, 17; Jeremías 25, 15; Hab. 2, 16, etc.\f*Así también tú te embriagarás,
\p y desaparecerás;
\p también tú buscarás un refugio contra el enemigo.
\s Nínive no será restaurada jamás
\p
\v 12 Todas tus fortalezas
\p son higueras con brevas maduras,
\p que sacudidas caen en la boca
\p del que las va a comer.
\p
\v 13 He aquí que el pueblo
\p que está en medio de ti es como mujeres;
\p las puertas de tu país
\p se abren de par en par a tus enemigos;
\p el fuego devora tus cerrojos.
\p
\v 14 \f + \fr 14 s. \ft Invitación irónica a Nínive a prepararse para afrontar el asedio. Teniendo las murallas treinta metros de altura y un espesor de quince metros, se necesitaba un inmenso número de ladrillos para repararlas.\f*¡Sácate agua para el asedio,
\p refuerza tus baluartes;
\p entra en el lodo, pisa el barro,
\p toma el molde de ladrillos!
\p
\v 15 Allí te consumirá el fuego,
\p te destruirá la espada;
\p te devorará como devora la langosta.
\p ¡Multiplícate como la langosta,
\p hazte numerosa como la langosta!
\p
\v 16 \f + \fr 16. \ft Todas las riquezas de tus comerciantes serán semejantes a las langostas que en un momento parecen innumerables y en otro desaparecen de repente trasladándose a otra parte. Como los comerciantes, así desaparecen también los capitanes y defensores cuyo número era tan grande como un ejército de langostas.\f*Aumenta el número de tus traficantes
\p para que sean más numerosos
\p que las estrellas del cielo:
\p la langosta muda la piel y se va.
\p
\v 17 Tus príncipes son como langostas
\p y tus funcionarios como una manga de langostas;
\p se posan en los vallados
\p en un día de frío;
\p mas cuando se levanta el sol,
\p se huyen, y no se conoce
\p el lugar donde están.
\p
\v 18 \f + \fr 18. \ft \it Tus pastores duermen:\it* tus reyes y príncipes han perecido. En sentido espiritual Jonás fue pastor de Nínive. Cuando los pastores se duermen perece el rebaño. Véase versículo 4 y nota sobre la apostasía de Nínive.\f*Tus pastores, oh rey de Asiria, duermen;
\p tus nobles descansan (en el sepulcro),
\p tu pueblo anda disperso sobre los montes,
\p y no hay quien lo congregue.
\p
\v 19 No hay remedio para tu ruina;
\p tu herida es gravísima;
\p cuantos oyeren hablar de tu \it (ruina)\it*,
\p batirán palmas contra ti;
\p pues ¿sobre quién no pasó
\p de continuo tu maldad?
