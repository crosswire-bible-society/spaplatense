# EsPlatense
[es] La Biblia Platense, también conocida como Biblia Comentada, es una traducción católica de la biblia al español realizada por el sacerdote católico alemán Juan Straubinger. Es llamada Platense por haberse hecho en la ciudad de La Plata, Argentina.

[en] The Platense Bible, also known as the Commented Bible, is a Catholic translation of the Bible into Spanish by the German Catholic priest Juan Straubinger. It is called Platense for having been done in the city of La Plata, Argentina.
Source text: https://ia800407.us.archive.org/29/items/SagradaBibliaStraubinger/SagradaBibliaStraubinger.html
